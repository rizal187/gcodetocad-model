# -*- coding: utf-8 -*-
# Author- Khem Raj Rijal
# Description-Generates Solid CAD model from GCode.

import adsk.core, adsk.fusion, traceback
import re
import time


handlers = []
phandlers = []
defaultName = 'My Part'
defaultMajorRadius = 0.035
defaultMinorRadius = 0.035
defaultNumPoints = '10'
data = []

majorRadius = 0
minorRadius = 0

numPoints = 0

app = adsk.core.Application.get()
if app:
    ui = app.userInterface



class PartCommandExecuteHandler(adsk.core.CommandEventHandler):
    def __init__(self):
        super().__init__()
    def notify(self, args):
        try:
            unitsMgr = app.activeProduct.unitsManager
            command = args.firingEvent.sender
            inputs = command.commandInputs
            global numPoints
            

            for input in inputs:
                if input.id == 'partName':
                    defaultName = input.value
                elif input.id == 'majorRadius':
                    defaultMajorRadius = unitsMgr.evaluateExpression(input.expression, "mm")
                elif input.id == 'minorRadius':
                    defaultMinorRadius = unitsMgr.evaluateExpression(input.expression, "mm")
                elif input.id == 'numPoints':
                    defaultNumPoints = input.value
                
            global numPoints, majorRadius,minorRadius
            numPoints = str(defaultNumPoints)
            majorRadius = defaultMajorRadius
            minorRadius = defaultMinorRadius
            
            
            args.isValidResult = True

        except:
            if ui:
                ui.messageBox('Failed:\n{}'.format(traceback.format_exc()))

class PartCommandDestroyHandler(adsk.core.CommandEventHandler):
    def __init__(self):
        super().__init__()
    def notify(self, args):
        try:
            # when the command is done, terminate the script
            # this will release all globals which will remove all event handlers
            createBodies(numPoints)
            
            global phandlers,data 
            phandlers = []
            data = []
            
        except:
            if ui:
                ui.messageBox('Failed:\n{}'.format(traceback.format_exc()))

class PartCommandCreatedHandler(adsk.core.CommandCreatedEventHandler):    
    def __init__(self):
        super().__init__()        
    def notify(self, args):
        try:
            cmd = args.command
            cmd.isRepeatable = True
            onExecute = PartCommandExecuteHandler()
            cmd.execute.add(onExecute)
            onExecutePreview = PartCommandExecuteHandler()
            cmd.executePreview.add(onExecutePreview)
            onDestroy = PartCommandDestroyHandler()
            cmd.destroy.add(onDestroy)

            # keep the handler referenced beyond this function
            phandlers.append(onExecute)
            phandlers.append(onExecutePreview)
            phandlers.append(onDestroy)

            #define the inputs
            inputs = cmd.commandInputs

            inputs.addImageCommandInput('logo_image', '', './/Resources//BuildParts//logo.png')


            inputs.addStringValueInput('partName', 'Part Name', defaultName)

            inputs.addStringValueInput('maxPoints', 'Max Number of Points', str(len(data)-1))

            initMajorRadius = adsk.core.ValueInput.createByReal(defaultMajorRadius)
            inputs.addValueInput('majorRadius', 'Major Radius','mm',initMajorRadius)

            initMinorRadius = adsk.core.ValueInput.createByReal(defaultMinorRadius)
            inputs.addValueInput('minorRadius', 'Minor Radius', 'mm', initMinorRadius)
            
            
            inputs.addStringValueInput('numPoints','Number of Points' ,defaultNumPoints)
            cmd.okButtonText = 'Build Parts!'
            
        except:
            if ui:
                ui.messageBox('Failed:\n{}'.format(traceback.format_exc()))



def readGcode(filename):

    writefile = filename.split('.')[0] + '.txt'
    w = open(writefile,'w')
    k = 0
    global data 
    with open(filename, 'r') as f:
        for line in f:
            Z = re.search('G1\sZ(\d+.\d+)',line)
            if Z:
                z = Z.group(1)
            xyz = re.search('G1 X(\d+.\d+)\sY(\d+.\d+)\s[EF](\d+.\d+)',line)
            if xyz:
                if float(xyz.group(3))>0:
                    k+=1
                    x,y = xyz.group(1),xyz.group(2)
                    data.append([float(x)/10,float(y)/10,float(z)/10])
                    w.write(x+','+y+','+z+'\n')
    w.close()
    



def initFusionDoc():

    app = adsk.core.Application.get()

    # new document
    app.documents.add(adsk.core.DocumentTypes.FusionDesignDocumentType)
    des = adsk.fusion.Design.cast(app.activeProduct)

    # direct design
    des.designType = adsk.fusion.DesignTypes.DirectDesignType

    return des

def unionBodies(
    body :adsk.fusion.BRepBody,
    bodies :list
    ) -> list:

    tmpMgr = adsk.fusion.TemporaryBRepManager.get()
    unionBool = adsk.fusion.BooleanTypes.UnionBooleanType
    target :adsk.fusion.BRepBody = body
    lst = []
    for idx, tool in enumerate(bodies):
        try:
            tmpMgr.booleanOperation(target, tool, unionBool)
        except:
            lst.append(target)
            target = tool
    lst.append(target)

    return lst

def initCylinders(
    pnts :list,
    majorRadius :float,
    minorRadius :float
    ) -> list:

    tmpMgr = adsk.fusion.TemporaryBRepManager.get()
    cyls = []
    for idx in range(len(pnts)-1):
        # creating vector to orient epllipse/ circluar profile along the path
        vec = adsk.core.Vector3D.create(pnts[idx].x,pnts[idx].y,pnts[idx].z)

        #creating elliptical or circluar cylinder with major radius  and minor radius along the direction of vector coordinate
        cyl = tmpMgr.createEllipticalCylinderOrCone(
            pnts[idx],majorRadius,minorRadius, pnts[idx + 1], majorRadius, vec)


        cyl.isSolid=True
        cyl.isTransient=False

        if cyl:
            cyls.append(cyl)
    
    return cyls



def createBodies(numPoints):
    des = adsk.fusion.Design.cast(app.activeProduct)

     # direct design
    des.designType = adsk.fusion.DesignTypes.DirectDesignType

     # time
    startTime = time.time()

    global data, majorRadius,minorRadius
    pnt3D = adsk.core.Point3D
    dt = data[:int(numPoints)]
    pnts = [pnt3D.create(pt[0], pt[1], pt[2]) for pt in dt]

        # create cylinder
    
    cylinders = initCylinders(pnts, majorRadius,minorRadius)

    allBody = unionBodies(cylinders[0], cylinders[1:])
    print('unionBodies:{}'.format(time.time() - startTime))

        # add bodies
    splitLeng = 100
    import itertools
    bodyLst = [d for d in itertools.zip_longest(*[iter(allBody)] * splitLeng)]

    for bodies in bodyLst:
        des = initFusionDoc()
        root = des.rootComponent
        bRepBodies = root.bRepBodies

        for body in bodies:
            if not body: break
            bRepBodies.add(body)
            
        des.designType = adsk.fusion.DesignTypes.ParametricDesignType
        adsk.doEvents()

    print('add bodies:{}'.format(time.time() - startTime))
        

    msg = '-- Done --\nBodyCount : {}\nTime : {}'.format(
        len(cylinders),time.time() - startTime)
    ui.messageBox(msg)

class MakePartCommandCreatedEventHandler(adsk.core.CommandCreatedEventHandler):
    def __init__(self):
        super().__init__()
    def notify(self, args):
        ui = []
        try:
            # Instantiate Fusion360 Objects
            app = adsk.core.Application.get()
            ui = app.userInterface
            



            #Dialog box to read gcode file
            title = 'Select Gcode file generated by Slicer 3D'
            dlg = ui.createFileDialog()
            dlg.title = 'Open Gcode'
            dlg.filter = 'Gcode (*.gcode);;All Files (*.*)'
            if dlg.showOpen() != adsk.core.DialogResults.DialogOK :
                return
            filename = dlg.filename

            #Gets x,y,z coordinates from gcode file
            readGcode(filename)

            # Custom Input for major and minor Radius
            commandDefinitions = ui.commandDefinitions
            #check the command exists or not
            cmdDef = commandDefinitions.itemById('Part')
            if not cmdDef:
                cmdDef = commandDefinitions.addButtonDefinition('Part',
                    'GCode Reverse Modeling',
                    'Get Custom Input for major and minor radius with number of points.') 


           
            onCommandCreated = PartCommandCreatedHandler()
            cmdDef.commandCreated.add(onCommandCreated)
            # keep the handler referenced beyond this function


            global phandlers
            
            phandlers.append(onCommandCreated)
            inputs = adsk.core.NamedValues.create()
            cmdDef.execute(inputs)

        except:
            if ui:
                ui.messageBox('Failed:\n{}'.format(traceback.format_exc()))


def run(context):
    ui = None
    try:
        app = adsk.core.Application.get()
        ui  = app.userInterface


        if ui.commandDefinitions.itemById('MakePartButton'):
            ui.commandDefinitions.itemById('MakePartButton').deleteMe()

        cmdDefs = ui.commandDefinitions

        buttonMakePart = cmdDefs.addButtonDefinition('MakePartButton', 'Build Parts from Gcode', 'Generate solid models from Gcode file you get from Slicer software. It reverses sliced file (tool path) back into Solid CAD model which can be used for geometric and mechanical analyais.        Author : Khem Raj Rijal.                 ', './/Resources//BuildParts')
        buttonMakePart.toolClipFilename = './/Resources//BuildParts//logo.png'
        MakePartCommandCreated = MakePartCommandCreatedEventHandler()
        buttonMakePart.commandCreated.add(MakePartCommandCreated)
        handlers.append(MakePartCommandCreated)

        solidPanel = ui.allToolbarPanels.itemById('SolidMakePanel')
        surfacePanel = ui.allToolbarPanels.itemById('SurfaceMakePanel')
        buttonControl = solidPanel.controls.addCommand(buttonMakePart, '', False)
        buttonControl = surfacePanel.controls.addCommand(buttonMakePart, '',False)

    except:
        pass
       

def stop(context):
    ui = None
    try:
        app = adsk.core.Application.get()
        ui  = app.userInterface

        #Cleanup
        if ui.commandDefinitions.itemById('MakePartButton'):
            ui.commandDefinitions.itemById('MakePartButton').deleteMe()

        solidPanel = ui.allToolbarPanels.itemById('SolidMakePanel')
        cntrl = solidPanel.controls.itemById('MakePartButton')
        if cntrl:
            cntrl.deleteMe()

        surfacePanel = ui.allToolbarPanels.itemById('SurfaceMakePanel')
        cntrl = surfacePanel.controls.itemById('MakePartButton')
        if cntrl:
            cntrl.deleteMe()

    except:
        pass
        
